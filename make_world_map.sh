#!/bin/sh

# Copyright (C) 2020 - David A. Redick
# Under the GNU AGPLv3

# Makes a bare bones SVG map of the world.
# The handy in case you get lost.

# runs in the maps directory
# note that there are some directories for "submaps"
# which redick is ignoring... for now.
cd /home/redick/source/flare-game/mods/empyrean_campaign/maps

# output the graphviz dot preamble
echo 'digraph {' > world_map.dot

# grep returns things in this pattern:
# wizards_tower_1.txt:intermap=maps/black_oak_city.txt,46,50
#
# the first sed converts to:
# wizards_tower_1.txt:intermap=maps/black_oak_city;
#
# the second sed converts to:
# wizards_tower_1 -> black_oak_city;
grep 'intermap' *.txt | \
        sed -e 's/\.txt,.*/;/' | \
        sed -e 's/\.txt:intermap=maps\// -> /' \
        >> world_map.dot

# close off the graphviz dot file
echo '}' >> world_map.dot

# generate the svg using graphviz's dot program
dot world_map.dot -Tsvg > world_map.svg

# and display
firefox world_map.svg
